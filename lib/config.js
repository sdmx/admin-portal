const config = {
  status: ['Unverified', 'Active', 'Blocked'],
};

export default (path = null) => {
  if (path) {
    const env = process.env[path];

    if (env !== undefined) {
      return env;
    }

    let res = config;

    path.split('.').forEach((key) => {
      res = res[key];
    });

    return res;
  }

  return config;
};
