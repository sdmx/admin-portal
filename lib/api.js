import axios from 'axios';
import store from 'store';
import qs from 'qs';
import expirePlugin from 'store/plugins/expire';

store.addPlugin(expirePlugin);

let token = null;

axios.interceptors.request.use(
  (config) => {
    const { headers = {} } = config;

    headers.Authorization = `Bearer ${token}`;

    return { ...config, headers };
  },
  error => Promise.reject(error),
);

export const Content = {
  privilege: [
    { id: 'public', value: '1', icon: 'global', text: 'Public' },
    { id: 'internal', value: '2', icon: 'bank', text: 'Internal' },
  ],
  contentType: [
    { id: 'news', icon: 'file-text', text: 'News', className: 'bg-dark-red' },
    { id: 'publication', icon: 'notification', text: 'Publications', className: 'bg-dark-blue' },
  ],
  status: [
    { id: 'draft', value: '0', text: 'Draft', className: 'bg-light-gray black-80' },
    { id: 'submitted', value: '1', text: 'Submitted', className: 'bg-dark-pink white' },
    { id: 'approved', value: '2', text: 'Approved', className: 'bg-dark-green white' },
    { id: 'rejected', value: '3', text: 'Rejected', className: 'bg-dark-red white' },
  ],
  get: (contentType, privilege, id) =>
    axios.get(`${process.env.CMS_ENDPOINT}/content/${contentType}/${privilege}/${id}`),
  list: (contentType, privilege, limit = 10, offset = 0) =>
    axios.get(`${process.env.CMS_ENDPOINT}/content/${contentType}/${privilege}`, {
      params: { limit, offset },
    }),
  create: (contentType, privilege, formData) =>
    axios.post(`${process.env.CMS_ENDPOINT}/content/${contentType}/${privilege}`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }),
  update: (contentType, privilege, formData, id) =>
    axios.put(`${process.env.CMS_ENDPOINT}/content/${contentType}/${privilege}/${id}`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }),
  delete: (contentType, privilege, id) =>
    axios.delete(`${process.env.CMS_ENDPOINT}/content/${contentType}/${privilege}/${id}`),
};

export const Organisation = {
  list: () =>
    axios.get(`${process.env.CMS_ENDPOINT}/organisation`),
  positions: () =>
    axios.get(`${process.env.CMS_ENDPOINT}/position`),
};

export const Approval = {
  list: (limit, offset) =>
    axios.get(`${process.env.CMS_ENDPOINT}/approval`, { params: { limit, offset } }),
  get: id =>
    axios.get(`${process.env.CMS_ENDPOINT}/approval/${id}`),
  process: (processName, id, formData) =>
    axios.put(`${process.env.CMS_ENDPOINT}/approval/${id}/${processName}`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }),
  reject: (id, formData) =>
    Approval.process('reject', id, formData),
  approve: (id, formData) =>
    Approval.process('approve', id, formData),
};

export const ApprovalFlow = {
  list: () =>
    axios.get(`${process.env.CMS_ENDPOINT}/approval/flow`),
  get: id =>
    axios.get(`${process.env.CMS_ENDPOINT}/approval/flow/${id}`),
  config: (contentType, formData) =>
    axios.post(`${process.env.CMS_ENDPOINT}/approval/flow/${contentType}`, formData, {
      headers: { 'Content-Type': 'multipart/form-data' },
    }),
};

export const WsConfig = {
  list: () =>
    axios.get('/api/ws-config'),
  get: id =>
    axios.get(`/api/ws-config/${id}`),
  save: data =>
    axios.post('/api/ws-config', qs.stringify(data), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }),
};

export const Metadata = {
  list: () =>
    axios.get(`${process.env.METADATA_ENDPOINT}/all`),
  getStructure: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/structure/${id}`),
  getData: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/data/${id}`),
  getDataflowMapping: id =>
    axios.get(`${process.env.METADATA_ENDPOINT}/map/${id}`),
  saveMapping: (id, data) =>
    axios.post(`${process.env.METADATA_ENDPOINT}/map/${id}`, qs.stringify(data), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }),
  save: (id, data) =>
    axios.post(`${process.env.METADATA_ENDPOINT}/save/${id}`, qs.stringify(data), {
      headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    }),
};

export const Auth = {
  init: () =>
    new Promise((resolve, reject) => {
      const cache = store.get('token');

      if (cache) {
        resolve(cache);
        Auth.setToken(cache);
      }
      else {
        Auth.getToken()
          .then(({ data }) => {
            resolve(data);
            store.set('token', data, new Date().getTime() + (1000 * 60 * 10));
            Auth.setToken(data);
          })
          .catch(err => reject(err));
      }
    }),
  user() {
    return new Promise((resolve, reject) => {
      const cache = store.get('user');

      if (cache) {
        resolve(cache);
      }
      else {
        axios.get('/auth/user')
          .then(({ data }) => {
            store.set('user', data, new Date().getTime() + (1000 * 60 * 10));
            resolve(data);
          })
          .catch(e => reject(e));
      }
    });
  },
  logout(doRedirect = true) {
    store.remove('user');
    store.remove('token');

    if (doRedirect) {
      window.location = '/auth/logout';
    }
  },
  token() {
    return new Promise((resolve, reject) => {
      axios.get('/auth/token')
        .then(({ data }) => resolve(data))
        .catch(err => reject(err));
    });
  },
  getRoles: () =>
    axios.get(`${process.env.AUTH_ENDPOINT}/auth/roles`),
  getToken: () =>
    axios.get('/auth/token'),
  setToken: (_token) => {
    token = _token;
  },
};
