FROM node:9-alpine

COPY . /app/soenda/admin-portal
WORKDIR /app/soenda/admin-portal

COPY .env.docker /app/soenda/admin-portal/.env
COPY config/app.docker.json /app/soenda/admin-portal/config/app.json

RUN sh -c "yarn"
# RUN sh -c "yarn build"

EXPOSE 3002
ENTRYPOINT ["sh", "-c", "yarn dev"]
# ENTRYPOINT ["sh", "-c", "yarn prod"]
