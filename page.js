import { createStore, applyMiddleware, combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import thunkMiddleware from 'redux-thunk';
import withRedux from 'next-redux-wrapper';
import { withRouter } from 'next/router';

const stores = initialState => createStore(
  combineReducers({ form }),
  initialState,
  applyMiddleware(thunkMiddleware),
);

// import stores from './stores';

// export default component => withAuth(withRedux(stores)(component));
export default component => withRedux(stores)(withRouter(component));
// export default component => withRouter(component);
