const path = require("path");
const fs = require('fs');
const _ = require('lodash');

const configFilepath = path.resolve('./config/ws.json');
const wsConfig = require(configFilepath);

module.exports = (app) => {
  app.get('/api/ws-config', function (req, res) {
    res.send(wsConfig);
  });

  app.get('/api/ws-config/:id', function (req, res) {
    const { id } = req.params;
    const config = _.find(wsConfig, { id });

    res.send(config);
  });

  app.post('/api/ws-config', function (req, res) {
    const { name, endpoint } = req.body;
    const id = req.body.id || _.kebabCase(name);

    _.remove(wsConfig, item => item.id === id);
    wsConfig.push({ id, name, endpoint });

    fs.writeFile(configFilepath, JSON.stringify(wsConfig), (err) => {
      if (err) {
        return res.send(err.message, 500);
      }

      return res.send('OK');
    });
  });
}
