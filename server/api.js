const apiWsConfig = require('./api/ws-config');

module.exports = (app) => {
  apiWsConfig(app);
}
