const nextRoutes = require('next-routes')
const routes = module.exports = nextRoutes();

routes.add('/user/edit/:id', '/user/edit');
routes.add('/role/edit/:id', '/role/edit');
routes.add('/category/edit/:id', '/category/edit');

routes.add('/content/:contentType/:privilege', '/content/index');
routes.add('/content/:contentType/:privilege/create', '/content/create');
routes.add('/content/:contentType/:privilege/edit/:id', '/content/edit');

routes.add('/user/:userType', '/user/index');
routes.add('/user/:userType/create', '/user/create');
routes.add('/user/:userType/edit/:uid', '/user/edit');

routes.add('/approval/config/:posId/:contentType?', '/approval/config/index');
routes.add('/approval/show/:id', '/approval/show');

routes.add('/metadata/edit/:id', '/metadata/edit');
routes.add('/metadata/map/:id', '/metadata/map');
