import React from 'react';
import nprogress from 'nprogress';
import { message } from 'antd';
import Router from 'next/router';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Form from '~/components/Metadata/FormMapping';
import { Metadata } from '~/lib/api';
import page from '~/page';

class MappingPage extends React.Component {
  static getInitialProps(req) {
    return { id: req.query.id };
  }

  constructor(props) {
    super(props);
    this.state = {
      msdList: [],
      data: {},
      loading: true,
    };
  }

  componentDidMount() {
    const { id } = this.props;

    this.fetchMsdList(id);
    this.fetchDataflowMapping(id);
  }

  fetchMsdList() {
    Metadata.list()
      .then(({ data }) => {
        this.setState({ msdList: data });
      })
      .catch((err) => {
        message.error(err.message);
      });
  }

  fetchDataflowMapping(id) {
    Metadata.getDataflowMapping(id)
      .then(({ data }) => {
        this.setState({ data, loading: false });
      })
      .catch(() => {
        this.setState({ loading: false });
      });
  }

  render() {
    const { id } = this.props;
    const { loading, msdList, data } = this.state;

    return (
      <Layout>
        <Loading isFinish={!loading}>
          <Form
            id={id}
            msdList={msdList}
            data={data}
            onSubmit={(values) => {
              nprogress.start();

              Metadata.saveMapping(id, values)
                .then(() => {
                  nprogress.done();
                  Router.push('/metadata');
                })
                .catch((err) => {
                  message.error(err.message);
                  nprogress.done();
                });
            }}
          />
        </Loading>
      </Layout>
    );
  }
}

MappingPage.defaultProps = {
  data: {},
};

export default page(MappingPage);
