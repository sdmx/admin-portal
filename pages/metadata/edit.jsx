import React from 'react';
import nprogress from 'nprogress';
import { message } from 'antd';
import Router from 'next/router';

import { Metadata } from '~/lib/api';
import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Form from '~/components/Metadata/FormMetadata';
import page from '~/page';

class EditPage extends React.Component {
  static getInitialProps(req) {
    return { id: req.query.id };
  }

  constructor(props) {
    super(props);
    this.state = {
      msd: {},
      data: {},
      loading: true,
    };
  }

  componentDidMount() {
    const { id } = this.props;

    this.fetchMsd(id);
    this.fetchDataset(id);
  }

  fetchMsd(id) {
    Metadata.getStructure(id)
      .then(({ data }) => {
        this.setState({ msd: data });
      })
      .catch((err) => {
        message.error(err.message);
      });
  }

  fetchDataset(id) {
    Metadata.getData(id)
      .then(({ data }) => {
        this.setState({ data, loading: false });
      })
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { id } = this.props;
    const { loading, msd, data } = this.state;

    return (
      <Layout>
        <Loading isFinish={!loading}>
          <Form
            id={id}
            msd={msd}
            data={data}
            onSubmit={(values) => {
              nprogress.start();
              this.setState({ loading: true });

              Metadata.save(id, values)
                .then(() => {
                  nprogress.done();
                  Router.push('/metadata');
                })
                .catch((err) => {
                  message.error(err.message);
                  nprogress.done();
                  this.setState({ loading: false });
                });
            }}
          />
        </Loading>
      </Layout>
    );
  }
}

EditPage.defaultProps = {
  data: {},
};

export default page(EditPage);
