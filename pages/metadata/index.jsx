import React from 'react';
import Table from 'react-table';
import { Tabs, message } from 'antd';

import Link from '~/components/Link';
import Layout from '~/components/Layout/Admin';
import { Metadata } from '~/lib/api';
import page from '~/page';

import dataflowSample from './dataflow-sample.json';

class MetadataPage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      msdList: [],
      dataflowList: [],
      loading: true,
    };
  }

  componentDidMount() {
    // fetch msd list
    Metadata.list()
      .then(({ data }) => {
        this.setState({ msdList: data, loading: false });
      })
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });

    this.setState({ dataflowList: dataflowSample });
  }

  render() {
    const { msdList = [], dataflowList = [], loading } = this.state;

    return (
      <Layout>
        <div className="cf">
          <div className="fl w-70">
            <h1 className="fw2 ma0">Metadata Management</h1>
          </div>
        </div>
        <hr className="mt0" />

        <Tabs defaultActiveKey="1" onChange={tabActive => this.setState({ tabActive })}>
          <Tabs.TabPane tab="Metadata" key="metadata">
            <Table
              filterable
              pageSize={10}
              defaultPageSize={10}
              data={msdList}
              loading={loading}
              className="f6"
              columns={[
                { Header: 'ID', accessor: 'id' },
                { Header: 'Name', accessor: 'name' },
                {
                  Header: 'Action',
                  width: 100,
                  Cell: ({ original }) => (
                    <div className="tc">
                      <Link
                        button
                        href={`/metadata/edit/${original.id}`}
                        type="default"
                        icon="edit"
                        className="mr2"
                      />
                    </div>
                  ),
                },
              ]}
            />
          </Tabs.TabPane>
          <Tabs.TabPane tab="Dataflow Mapping" key="dataflow">
            <Table
              filterable
              pageSize={10}
              defaultPageSize={10}
              data={dataflowList}
              loading={loading}
              className="f6"
              columns={[
                { Header: 'ID', accessor: 'id' },
                { Header: 'Name', accessor: 'name' },
                {
                  Header: 'Action',
                  width: 100,
                  Cell: ({ original }) => (
                    <div className="tc">
                      <Link
                        button
                        href={`/metadata/map/${original.id}`}
                        type="default"
                        icon="edit"
                        className="mr2"
                      />
                    </div>
                  ),
                },
              ]}
            />
          </Tabs.TabPane>
        </Tabs>
      </Layout>
    );
  }
}

export default page(MetadataPage);
