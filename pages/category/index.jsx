import React from 'react';
import { Popconfirm, Button, message } from 'antd';
import Link from '~/components/Link';
import Table from 'react-table';
import axios from 'axios';
import nprogress from 'nprogress';
import _ from 'lodash';

import Layout from '~/components/Layout/Admin';
import page from '~/page';

class DataList extends React.Component {
  static getInitialProps({ query }) {
    return {
      endpoint: `${process.env.CMS_ENDPOINT}/category`,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: false,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page, sorted, filtered }) {
    this.setState({ loading: true });
    console.log(this.props.endpoint)

    axios
      .get(this.props.endpoint, {
        params: {
          limit: pageSize,
          offset: page * pageSize,
        },
      })
      .then(({ data }) => {
        this.setState({
          loading: false,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
        })
      })
      .catch(res => this.setState({ loading: false }))
  }

  render() {
    const { endpoint } = this.props;
    const { data, totalPages, loading } = this.state;

    return (
      <Layout>
        <div className="cf">
          <div className="fl w-70">
            <h1 className="fw2 ma0">Category</h1>
          </div>
          <div className="fl w-30 tr">
            <Link button href={`/category/create`} icon="plus" type="primary">
              Create
            </Link>
          </div>
        </div>
        <hr className="mt0" />

        <Table
          manual
          filterable
          pageSize={10}
          defaultPageSize={10}
          data={data}
          pages={totalPages}
          loading={loading}
          onFetchData={this.fetchData}
          className="f6"
          columns={[
            { Header: 'Name', accessor: 'name' },
            {
              Header: 'Action',
              width: 100,
              Cell: cell => (
                <div className="tc">
                  <Link
                    button
                    href={`/category/edit/${cell.original.id}`}
                    type="default"
                    icon="edit"
                    className="mr2"
                  />
                  <Popconfirm
                    placement="left"
                    title="Are you sure ?"
                    onConfirm={() => {
                      nprogress.start();

                      axios
                        .delete(`${endpoint}/${cell.original.id}`)
                        .then((res) => {
                          this.setState({
                            data: _.filter(data, item => item.id !== cell.original.id),
                          }, () => nprogress.done())
                        })
                        .catch((err) => {
                          console.log(err.message);
                          nprogress.done();
                        });
                    }}
                  >
                    <Button type="danger" icon="delete" />
                  </Popconfirm>
                </div>
              ),
            },
          ]}
        />
      </Layout>
    );
  }
}

export default page(DataList);
