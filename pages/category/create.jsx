import React from 'react';
import nprogress from 'nprogress';
import Router from 'next/router';
import axios from 'axios';

import Layout from '~/components/Layout/Admin';
import page from '~/page';

import Form, { formData } from '~/components/Category/Form';

export default page(() => (
  <Layout>
    <Form
      onSubmit={(values) => {
        nprogress.start();

        axios
          .post(`${process.env.CMS_ENDPOINT}/category`, formData(values), {
            headers: { 'Content-Type': 'multipart/form-data' },
          })
          .then(() => {
            nprogress.done();
            Router.push('/category');
          })
          .catch((err) => {
            nprogress.done();
          });
      }}
    />
  </Layout>
));
