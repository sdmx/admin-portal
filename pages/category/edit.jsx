import React from 'react';
import PropTypes from 'prop-types';
import nprogress from 'nprogress';
import Router from 'next/router';
import axios from 'axios';

import Layout from '~/components/Layout/Admin';
import page from '~/page';

import Form, { formData } from '~/components/Category/Form';

const EditPage = ({ data, id }) => (
  <Layout>
    <Form
      id={id}
      data={data}
      onSubmit={(values) => {
        nprogress.start();

        axios
          .put(`${process.env.CMS_ENDPOINT}/category/${data.id}`, formData(values), {
            headers: { 'Content-Type': 'multipart/form-data' },
          })
          .then(() => {
            nprogress.done();
            Router.push('/category');
          })
          .catch((err) => {
            console.log(err.message);
            nprogress.done();
          });
      }}
    />
  </Layout>
);

EditPage.propTypes = {
  data: PropTypes.objectOf(PropTypes.node),
};

EditPage.defaultProps = {
  data: {},
};

EditPage.getInitialProps = async ({ query }) => {
  const props = { id: query.id, data: {} };

  if (query.id) {
    const res = await axios.get(`${process.env.CMS_ENDPOINT}/category/${query.id}`);
    props.data = res.data;
  }

  return props;
};

export default page(EditPage);
