import React from 'react';
import Router from 'next/router';
import v from 'voca';
import { message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Form, { formData } from '~/components/Content/Form';
import { Content, Auth } from '~/lib/api';
import page from '~/page';

class EditPage extends React.Component {
  state = { data: {}, loading: true }

  static getInitialProps({ query }) {
    return {
      id: query.id,
      contentType: query.contentType,
      privilege: query.privilege,
    };
  }

  componentDidMount() {
    const { contentType, privilege, id } = this.props;

    Content.get(contentType, privilege, id)
      .then(({ data }) => this.setState({ data, loading: false }))
      .catch(err => message.error(err.message));
  }

  render() {
    const { id, contentType, privilege } = this.props;
    const { data, loading } = this.state;
    const FormComponent = Form[v.capitalize(v.camelCase(contentType))];

    return (
      <Layout>
        <Loading isFinish={!loading}>
          <FormComponent
            id={id}
            data={data}
            title={"Edit " + v.titleCase(contentType)}
            onSubmit={(values) => {
              this.setState({ loading: true });

              Content.update(contentType, privilege, formData(values), id)
                .then(() => {
                  Router.push(`/content/${contentType}/${privilege}`);
                })
                .catch((err) => {
                  this.setState({ loading: false });
                  message.error(err.message);
                });
            }}
          />
        </Loading>
      </Layout>
    );
  }
}

export default page(EditPage);
