import React from 'react';
import Router from 'next/router';
import v from 'voca';
import { message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Form, { formData } from '~/components/Content/Form';
import { Content } from '~/lib/api';
import page from '~/page';

class CreatePage extends React.Component {
  state = { loading: false }

  static async getInitialProps({ query }) {
    return {
      contentType: query.contentType,
      privilege: query.privilege,
    };
  }

  render() {
    const { contentType, privilege } = this.props;
    const { loading } = this.state;

    const FormComponent = Form[v.capitalize(v.camelCase(contentType))];

    return (
      <Layout>
        <Loading isFinish={!loading}>
          <FormComponent
            title={"Create " + v.titleCase(contentType)}
            onSubmit={(values) => {
              this.setState({ loading: true });

              Content.create(contentType, privilege, formData(values))
                .then(() => {
                  Router.push(`/content/${contentType}/${privilege}`);
                })
                .catch((err) => {
                  this.setState({ loading: false });
                  message.error(err.message);
                });
            }}
          />
        </Loading>
      </Layout>
    );
  }
}

export default page(CreatePage);
