import React from 'react';
import { Popconfirm, Button, message, Icon } from 'antd';
import Link from '~/components/Link';
import Table from 'react-table';
import striptags from 'striptags';
import moment from 'moment';
import nprogress from 'nprogress';
import _ from 'lodash';
import v from 'voca';

import Layout from '~/components/Layout/Admin';
import PageHeader from '~/components/PageHeader';
import { Content } from '~/lib/api';
import page from '~/page';

class DataList extends React.Component {
  static getInitialProps({ query }) {
    const title = {
      news: 'News',
      publication: 'Publication',
      legal: 'Regulation',
    };

    return {
      pageTitle: title[query.contentType],
      contentType: query.contentType,
      privilege: query.privilege,
    };
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: false,
    };

    // Auth.init();
    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page: pageNum }) {
    const { contentType, privilege } = this.props;

    this.setState({ loading: true });

    Content.list(contentType, privilege, pageSize, pageNum * pageSize)
      .then(({ data }) => {
        this.setState({
          loading: false,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
        });
      })
      .catch((err) => {
        this.setState({ loading: false });
        message.error(err.message);
      });
  }

  render() {
    const { pageTitle, contentType, privilege } = this.props;
    const { data, totalPages, loading } = this.state;

    return (
      <Layout>
        <PageHeader
          title={pageTitle}
          action={
            <Link
              button
              icon="plus"
              type="primary"
              href={`/content/${contentType}/${privilege}/create`}
            >
              Create
            </Link>
          }
        />

        <Table
          manual
          filterable
          pageSize={10}
          defaultPageSize={10}
          data={data}
          pages={totalPages}
          loading={loading}
          onFetchData={this.fetchData}
          className="f6"
          columns={[
            { Header: 'Name', accessor: 'name', width: 300 },
            { Header: 'Content', Cell: ({ original }) => striptags(original.body) },
            {
              Header: 'Created',
              accessor: 'created',
              className: 'tc',
              width: 150,
              Cell: ({ original }) => moment(original.created).format('DD MMM YYYY'),
            },
            {
              Header: 'Last Update',
              accessor: 'updated',
              className: 'tc',
              width: 150,
              Cell: ({ original }) => moment(original.updated).format('DD MMM YYYY'),
            },
            {
              Header: 'Status',
              accessor: 'status',
              className: 'tc',
              width: 150,
              Cell: ({ original }) => {
                const status = _.find(Content.status, i => i.value === original.status);

                return (
                  <div className={`pa1 br1 b f7 ${status.className}`}>
                    {status.text}
                  </div>
                );
              },
            },
            {
              Header: 'Action',
              width: 100,
              Cell: ({ original }) => (
                <div className="tc">
                  {/* Only Draft & Rejected */}
                  {['0', '3'].indexOf(original.status) !== -1 && (
                    <div>
                      <Link
                        button
                        href={`/content/${contentType}/${privilege}/edit/${original.id}`}
                        type="default"
                        icon="edit"
                        className="mr2"
                      />
                      <Popconfirm
                        placement="left"
                        title="Are you sure ?"
                        onConfirm={() => {
                          nprogress.start();

                          Content.delete(contentType, privilege, original.id)
                            .then(() => {
                              this.setState({
                                data: _.filter(data, item => item.id !== original.id),
                              }, () => nprogress.done());
                            })
                            .catch((err) => {
                              nprogress.done();
                              message.error(err.message);
                            });
                        }}
                      >
                        <Button type="danger" icon="delete" />
                      </Popconfirm>
                    </div>
                  )}
                </div>
              ),
            },
          ]}
        />
      </Layout>
    );
  }
}

export default page(DataList);
