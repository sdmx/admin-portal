import React from 'react';
import { Button, Icon, message } from 'antd';
import Table from 'react-table';

import { WsConfig } from '~/lib/api';
import Layout from '~/components/Layout/Admin';
import page from '~/page';

import FormModal from '~/components/WsConfig/FormModal';

class DataList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      loading: false,
      form: {
        id: null,
        data: {},
        show: false,
      },
    };

    this.fetchData = this.fetchData.bind(this);
    this.openWsConfigForm = this.openWsConfigForm.bind(this);
    this.closeWsConfigForm = this.closeWsConfigForm.bind(this);
    this.saveWsConfig = this.saveWsConfig.bind(this);
  }

  fetchData() {
    this.setState({ loading: true });

    WsConfig.list()
      .then(({ data }) => this.setState({ data, loading: false }))
      .catch(() => this.setState({ loading: false }));
  }

  openWsConfigForm(id) {
    WsConfig.get(id).then(({ data }) => {
      this.setState({ form: { id, data, show: true } });
    });
  }

  closeWsConfigForm() {
    this.setState({ form: { id: null, data: {}, show: false } });
  }

  saveWsConfig(values) {
    console.log('values: ', values);
    WsConfig.save(values)
      .then(() => this.closeWsConfigForm())
      .catch((err) => {
        message.error(err.message);
        this.closeWsConfigForm();
      });
  }

  render() {
    const { data, loading, form } = this.state;

    return (
      <Layout>
        <div className="cf">
          <div className="fl w-70">
            <h1 className="fw2 ma0">Web Service Configuration</h1>
          </div>
        </div>
        <hr className="mt0" />

        <Table
          manual
          filterable
          pageSize={10}
          defaultPageSize={10}
          data={data}
          loading={loading}
          onFetchData={this.fetchData}
          className="f6"
          columns={[
            { Header: 'Name', accessor: 'name' },
            {
              Header: 'Action',
              width: 100,
              Cell: ({ original }) => (
                <div className="tc">
                  <Button type="default" onClick={() => this.openWsConfigForm(original.id)}>
                    <Icon type="edit" />
                  </Button>
                </div>
              ),
            },
          ]}
        />

        <FormModal
          {...form}
          onCloseModal={this.closeWsConfigForm}
          onSubmit={this.saveWsConfig}
        />
      </Layout>
    );
  }
}

export default page(DataList);
