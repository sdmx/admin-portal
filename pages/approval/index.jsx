import React from 'react';
import { Popconfirm, Button, message } from 'antd';
import Table from 'react-table';
import axios from 'axios';
import nprogress from 'nprogress';
import moment from 'moment';
import _ from 'lodash';

import { Approval } from '~/lib/api';
import Link from '~/components/Link';
import Layout from '~/components/Layout/Admin';
import page from '~/page';

class DataList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: false,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page: pageNum }) {
    this.setState({ loading: true });

    Approval.list(pageSize, pageNum * pageSize)
      .then(({ data }) => {
        this.setState({
          loading: false,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
        });
      })
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { data, totalPages, loading } = this.state;

    return (
      <Layout>
        <div className="flex">
          <div className="w-60">
            <h1 className="fw2 ma0">Approval</h1>
          </div>
          <div className="w-40 tr">
            <Link href="/approval/config">
              <Button type="primary" icon="setting"> Configuration </Button>
            </Link>
          </div>
        </div>
        <hr className="mt1" />

        <Table
          manual
          filterable
          pageSize={10}
          defaultPageSize={10}
          data={data}
          pages={totalPages}
          loading={loading}
          onFetchData={this.fetchData}
          className="f6"
          columns={[
            {
              Header: 'Name',
              Cell: cell => _.get(cell.original, '[1].name'),
            },
            {
              Header: 'Content Type',
              className: 'tc',
              Cell: cell => _.get({
                p: 'Publication',
                n: 'News',
                l: 'Regulation',
              }, _.get(cell.original, '[0].contentType')),
            },
            {
              Header: 'Created',
              accessor: 'created',
              className: 'tc',
              width: 150,
              Cell: (cell) => {
                const created = _.get(cell.original, '[0].created');
                return created ? moment(created).format('DD MMM YYYY') : '-';
              },
            },
            {
              Header: 'Action',
              width: 100,
              Cell: (cell) => {
                const id = _.get(cell.original, '[0].id');
                return (
                  <div className="tc">
                    <Link
                      button
                      href={`/approval/show/${id}`}
                      type="default"
                      icon="edit"
                      className="mr2"
                    />
                  </div>
                );
              },
            },
          ]}
        />
      </Layout>
    );
  }
}

export default page(DataList);
