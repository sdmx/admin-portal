import React from 'react';
import nprogress from 'nprogress';
import Router from 'next/router';
import { message } from 'antd';
import v from 'voca';
import _ from 'lodash';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Form, { formData } from '~/components/Approval/Form';
import { Approval, Auth as AuthHttp } from '~/lib/api';
import page from '~/page';

class ShowContent extends React.Component {
  static getInitialProps({ query }) {
    return { id: query.id };
  }

  constructor(props) {
    super(props);

    this.fetchData = this.fetchData.bind(this);

    this.state = {
      data: {
        approval: {},
        content: {},
        categories: [],
        attachments: [],
      },
      loading: true,
    };
  }

  componentDidMount() {
    AuthHttp.init()
      .then(() => {
        this.fetchData();
      })
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });
  }

  fetchData() {
    Approval.get(this.props.id)
      .then(({ data }) => this.setState({ data, loading: false }))
      .catch((err) => {
        message.error(err.message);
        this.setState({ loading: false });
      });
  }

  render() {
    const { id } = this.props;
    const { loading, data } = this.state;
    const contentType = _.get({
      n: 'news',
      p: 'publication',
      l: 'legal',
    }, data.approval.contentType);
    const FormComponent = Form[v.capitalize(v.camelCase(contentType))];

    return (
      <Layout>
        <Loading isFinish={!loading}>
          {FormComponent ? (
            <FormComponent
              id={id}
              data={data}
              title={v.titleCase(contentType)}
              onSubmit={({ approve, ...values }) => {
                nprogress.start();

                const process = approve
                  ? Approval.approve(id, formData(values))
                  : Approval.reject(id, formData(values));

                process
                  .then(() => {
                    nprogress.done();
                    Router.push('/approval');
                  })
                  .catch((err) => {
                    nprogress.done();
                    message.error(err.message);
                  });
              }}
            />
          ) : 'Not Found'}
        </Loading>
      </Layout>
    );
  }
}

export default page(ShowContent);
