import React from 'react';
import nprogress from 'nprogress';
import Router from 'next/router';
import axios from 'axios';
import _ from 'lodash';
import { Menu, Icon, message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Link from '~/components/Link';
import Loading from '~/components/Loading';
import Form, { formData } from '~/components/Approval/FormConfig';
import { ApprovalFlow, Organisation, Auth } from '~/lib/api';
import page from '~/page';

class ApprovalConfiguration extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      positions: [],
      organisations: [],
      defPosId: null,
      defContentType: null,
      loading: true,
      flow: {},
      user: {},
    };

    this.contentType = {
      publication: { label: 'Publication', icon: 'sound' },
      news: { label: 'News', icon: 'file-text' },
      // legal: { label: 'Regulation', icon: 'check' },
    };

    this.fetchDataFlow = this.fetchDataFlow.bind(this);
  }

  static getInitialProps({ query: { posId, contentType } }) {
    return { posId, contentType };
  }

  componentDidMount() {
    Auth.user().then(user => this.setState({ user }));
    this.fetchDataFlow();
  }

  fetchDataFlow() {
    axios.all([Organisation.list(), Organisation.positions()])
      .then(axios.spread(({ data: organisations }, { data: positions }) => {
        const sortedPos = _.sortBy(positions.data, ['id']);
        const defaultPos = _.size(sortedPos) > 0 ? _.head(sortedPos).id.toString() : null;
        const {
          posId = defaultPos,
          contentType = _.head(Object.keys(this.contentType)),
        } = this.props;

        this.setState({
          positions: sortedPos,
          organisations: _.sortBy(organisations.data, ['id']),
          defPosId: posId,
          defContentType: contentType,
        });

        ApprovalFlow.get(posId)
          .then(({ data }) => {
            const flow = {};

            data.forEach((item) => {
              flow[item.type] = item;
            });

            this.setState({ flow, loading: false });
          })
          .catch((err) => {
            this.setState({ loading: false });
            message.error(err.message);
          });
      }))
      .catch((err) => {
        this.setState({ loading: false });
        message.error(err.message);
      });
  }

  render() {
    const {
      positions, organisations, loading, defPosId, defContentType, flow, user,
    } = this.state;

    return (
      <Layout>
        <div className="flex">
          <div className="w-20">
            <Menu
              mode="inline"
              style={{ height: '100%', borderRight: 0 }}
              selectedKeys={[defPosId]}
            >
              {positions.map(item => (
                <Menu.Item key={item.id}>
                  <Link href={`/approval/config/${item.id}`} className="tr">
                    {item.name}
                  </Link>
                </Menu.Item>
              ))}
            </Menu>
          </div>
          <div className="w-80 bl pl3 b--moon-gray">
            <Menu mode="horizontal" selectedKeys={[defContentType]}>
              {Object.entries(this.contentType).map(([type, contentType]) => (
                <Menu.Item key={type}>
                  <Link href={`/approval/config/${defPosId}/${type}`} className="tr">
                    <Icon type={contentType.icon} /> {contentType.label}
                  </Link>
                </Menu.Item>
              ))}
            </Menu>

            <Loading isFinish={!loading}>
              <Form
                orgId={user.orgId}
                posId={defPosId}
                data={_.get(flow, defContentType)}
                organisations={organisations.map(i => ({ value: i.id, text: i.name }))}
                positions={positions.map(i => ({ value: i.id, text: i.name }))}
                onSubmit={(values) => {
                  this.setState({ loading: true });
                  nprogress.start();

                  ApprovalFlow.config(defContentType, formData(values))
                    .then(() => {
                      nprogress.done();
                      Router.push(`/approval/config/${defPosId}/${defContentType}`);
                    })
                    .catch((err) => {
                      nprogress.done();
                      this.setState({ loading: false });
                      message.error(err.message);
                    });
                }}
              />
            </Loading>
          </div>
        </div>
      </Layout>
    );
  }
}

export default page(ApprovalConfiguration);
