import React from 'react';
import { Button, Form, Input } from 'antd';

import Layout from '~/components/Layout/Admin';
import Link from '~/components/Link';
import PageHeader from '~/components/PageHeader';

import page from '~/page';

import indonesia from '~/data/indonesia/indonesia.json';
import sampleData from '~/logs/dataflows.json';

class PageGeo extends React.Component {
  state = {
    searchData: null,
    activeData: null,
    searchLocation: null,
    activeLocations: [],
    dataflows: [],
    datamap: {},
  }

  componentDidMount() {
    this.setState({ dataflows: sampleData.content });
  }

  filterData = (searchData) => {
    this.setState({ searchData });
  }

  filterLocation = (searchLocation) => {
    this.setState({ searchLocation });
  }

  setActiveData = (activeData) => {
    this.setState({ activeData });
  }

  setActiveLocation = (activeLocation) => {
    const { activeLocations } = this.state;

    this.setState({ activeLocations: [...activeLocations, activeLocation] });
  }

  render() {
    const { activeData, activeLocations, dataflows, searchData, searchLocation } = this.state;
    const pageTitle = "Map";

    let locations = indonesia;

    if (searchData) {
      dataflows = dataflows.filter(({ id, name }) => {
        return name.toLowerCase().indexOf(searchData) !== -1
            || id.toLowerCase().indexOf(searchData) !== -1;
      });
    }

    if (searchLocation) {
      locations = locations
        .map((item) => {
          return {
            ...item,
            cities: item.cities.filter(({ name }) => {
              return name.toLowerCase().indexOf(searchLocation) !== -1;
            }),
          };
        })
        .filter((item) => {
          return item.cities.length > 0;
        });
    }

    return (
      <Layout>
        <PageHeader title={pageTitle} />

        <div className="flex">
          <div className="w-50">
            <Input.Search placeholder="Search..." onSearch={(q) => this.filterData(q)} allowClear />

            <div className="mt2">
              {dataflows.map(({ id, name, version }) => (
                <div key={id}
                    className={`pointer b--moon-gray pa2 bt ${id == activeData ? "bg-light-yellow" : "hover-bg-light-gray"}`}
                    onClick={() => this.setActiveData(id)}>
                  <div>{name}</div>
                  <div className="i f7 mt-2 gray tr">{id}:{version}</div>
                </div>
              ))}
            </div>
          </div>

          <div className="w-50 pl3">
            <div className="flex">
              <Input.Search placeholder="Search..." onSearch={(q) => this.filterLocation(q)} allowClear />
              <Button type="primary" className="ml2" onClick={() => {}}>Save</Button>
            </div>

            <div className="mt2">
              {locations.map(({ id: provId, province, cities }) => (
                <div key={provId} className="b--moon-gray default">
                  <div className="b pa2 bg-black-70 white"> {province} </div>
                  <div className="bt pl3">
                    {cities.map(({ id: cityId, name }) => (
                      <div key={cityId}
                          className={`pointer pa2 bb b--moon-gray ${activeLocations.indexOf(cityId) !== -1 ? "bg-light-yellow" : "hover-bg-light-gray"}`}
                          onClick={() => this.setActiveLocation(cityId)}>
                          {/*onClick={() => this.addMap(activeData, cityId)}>*/}
                        {name}
                      </div>
                    ))}
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}

export default page(PageGeo);
