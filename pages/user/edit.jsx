import React from 'react';
import Router from 'next/router';
import axios from 'axios';
import v from 'voca';
import { Checkbox, Icon, message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import page from '~/page';

import Form, { formData } from '~/components/User/Form';

class EditPage extends React.Component {
  state = { data: null, loading: false, categories: [] }

  static getInitialProps({ query }) {
    return {
      uid: query.uid,
      userType: query.userType,
    };
  }

  componentDidMount() {
    this.fetchData(this.props.uid);

    // category
    axios
      .get(`${process.env.CMS_ENDPOINT}/category`)
      .then(({ data }) => {
        const enabledCategories = data.data
            .filter(() => Math.round(Math.random()))
            .map(({ id }) => id);

        const categories = data.data.map(item => ({ ...item, enabled: enabledCategories.indexOf(item.id) !== -1 }));

        // set categories state
        this.setState({ categories });
      })
      .catch(res => this.setState({ loading: false }))
  }

  fetchData(uid) {
    axios
      .get(`${process.env.AUTH_ENDPOINT}/user/${this.props.userType}/${uid}`)
      .then(({ data }) => this.setState({ data }))
      .catch(err => message.error(err.message));
  }

  toggleCategory = (id) => {
    const { categories } = this.state;
    const category = categories.find(item => item.id == id);

    if (category) {
      category.enabled = !category.enabled;
    }

    this.setState({ categories: { ...categories } });
  }

  render() {
    const { uid, userType } = this.props;
    const { data, loading, categories } = this.state;
    const FormComponent = Form[userType];

    return (
      <Layout>
        <Loading isFinish={data && !loading}>
          <FormComponent
            id={uid}
            data={data}
            title="Edit User"
            onSubmit={(values) => {
              this.setState({ loading: true });

              axios
                .put(`${process.env.AUTH_ENDPOINT}/user/${userType}/${uid}`, formData(values), {
                  headers: { 'Content-Type': 'multipart/form-data' },
                })
                .then(() => {
                  Router.push(`/user/${userType}`);
                })
                .catch((err) => {
                  this.setState({ loading: false });
                  message.error(err.message);
                });
            }}
          />

          <h2 className="mt4 mb2 ph2 pb0 pt3 bt b--moon-gray">
            <Icon type="appstore" /> Categories
          </h2>
          <div className="pa3 ba br b--moon-gray cf">
            {categories.map(({ id, name, enabled }) => {
              return (
                <div className="fl w-20">
                  <Checkbox onChange={() => this.toggleCategory(id)} checked={enabled}>
                    {name}
                  </Checkbox>
                </div>
              );
            })}
          </div>
        </Loading>
      </Layout>
    );
  }
}

export default page(EditPage);
