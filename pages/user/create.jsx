import React from 'react';
import Router from 'next/router';
import axios from 'axios';
import v from 'voca';
import { message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import page from '~/page';

import Form, { formData } from '~/components/User/Form';

class CreatePage extends React.Component {
  state = { loading: false }

  static async getInitialProps({ query }) {
    return { userType: query.userType };
  }

  render() {
    const { userType } = this.props;
    const { loading } = this.state;

    const FormComponent = Form[userType];

    return (
      <Layout>
        <Loading isFinish={!loading}>
          <FormComponent
            title="Create User"
            onSubmit={(values) => {
              this.setState({ loading: true });

              axios
                .post(`${process.env.AUTH_ENDPOINT}/user/${userType}`, formData(values), {
                  headers: { 'Content-Type': 'multipart/form-data' },
                })
                .then(() => {
                  Router.push(`/user/${userType}`);
                })
                .catch((err) => {
                  this.setState({ loading: false });
                  message.error(err.message);
                });
            }}
          />
        </Loading>
      </Layout>
    );
  }
}

export default page(CreatePage);
