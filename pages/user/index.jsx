import React from 'react';
import Link from '~/components/Link';
import v from 'voca';
import { Icon } from 'antd';

import Layout from '~/components/Layout/Admin';
import Table from '~/components/User/Table';
import page from '~/page';

const title = {
  db: 'External',
  ldap: 'Internal',
};

class DataList extends React.Component {
  static getInitialProps({ query }) {
    return {
      userType: query.userType,
      endpoint: `${process.env.AUTH_ENDPOINT}/user/${query.userType}`,
    };
  }

  render() {
    const { endpoint, userType } = this.props;
    const DataTable = Table[userType];

    return (
      <Layout>
        <div className="cf">
          <div className="fl w-70">
            {/*<h1 className="fw2 ma0">{title[userType]}</h1>*/}
            <h1 className="fw2 ma0">User</h1>
          </div>
          <div className="fl w-30 tr">
            <Link button href={`/user/${userType}/create`} icon="plus" type="primary">
              Create
            </Link>
          </div>
        </div>

        <hr className="mt0" />

        <DataTable
          endpoint={endpoint}
          userType={userType}
        />
      </Layout>
    );
  }
}

export default page(DataList);
