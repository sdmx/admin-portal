import React from 'react';
import { Popconfirm, Button, message } from 'antd';
import Table from 'react-table';

import Link from '~/components/Link';
import Layout from '~/components/Layout/Admin';

import { Auth } from '~/lib/api';
import page from '~/page';

class PageRoleIndex extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: false,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page, sorted, filtered }) {
    this.setState({ loading: true });

    Auth.getRoles()
      .then(({ data }) => {
        this.setState({
          data: data,
          totalPages: 1,
          loading: false,
        });
      })
      .catch(res => this.setState({ loading: false }));
  }

  render() {
    const { data, totalPages, loading } = this.state;

    return (
      <Layout>
        <div className="cf">
          <div className="fl w-70">
            <h1 className="fw2 ma0">Roles</h1>
          </div>
          <div className="fl w-30 tr">
            <Link button href={`/role/create`} icon="plus" type="primary">
              Create
            </Link>
            <Link button href={`/role/config`} icon="setting" type="primary" className="ml3">
              Configuration
            </Link>
          </div>
        </div>
        <hr className="mt0" />

        <Table
          manual
          filterable
          pageSize={10}
          defaultPageSize={10}
          data={data}
          pages={totalPages}
          loading={loading}
          onFetchData={this.fetchData}
          className="f6"
          columns={[
            { Header: 'Name', accessor: 'name' },
            {
              Header: 'Action',
              width: 100,
              Cell: cell => (
                <div className="tc">
                  <Link
                    button
                    href={`/category/edit/${cell.original.id}`}
                    type="default"
                    icon="edit"
                    className="mr2"
                  />
                  <Popconfirm
                    placement="left"
                    title="Are you sure ?"
                    onConfirm={() => {}}
                  >
                    <Button type="danger" icon="delete" />
                  </Popconfirm>
                </div>
              ),
            },
          ]}
        />
      </Layout>
    );
  }
}

export default page(PageRoleIndex);
