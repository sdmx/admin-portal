import React from 'react';
import { Button, Icon, Switch, Table } from 'antd';

import Layout from '~/components/Layout/Admin';
import Link from '~/components/Link';
import PageHeader from '~/components/PageHeader';

import { Auth } from '~/lib/api';
import page from '~/page';

const permissionData = [
  {
    feature: "approval",
    name: "Approvals",
    permissions: ["approval", ],
  },
  {
    feature: "category",
    name: "Categories",
    permissions: ["view", "create", "edit", "delete", ],
  },
  {
    feature: "map",
    name: "Map",
    permissions: ["map", ],
  },
  {
    feature: "metadata",
    name: "Metadata",
    permissions: ["view", "create", "edit", "delete", ],
  },
  {
    feature: "news",
    name: "News",
    permissions: ["view", "create", "edit", "delete", ],
  },
  {
    feature: "publication",
    name: "Publications",
    permissions: ["view", "create", "edit", "delete", ],
  },
  {
    feature: "role",
    name: "Roles",
    permissions: ["view", "create", "edit", "delete", "configuration", ],
  },
  {
    feature: "user",
    name: "Users",
    permissions: ["view", "create", "edit", "delete", ],
  },
];

class PageRoleIndex extends React.Component {
  state = {
    rolePermissions: [],
    roles: [],
  }

  componentDidMount() {
    Auth.getRoles().then(({ data }) => {
      this.setState({ roles: data });
    });

    // set role x permission mapping
    const rolePermissions = [
      {
        role: 'admin',
        permissions: [
          "approval:approval",
          "category:view", "category:create", "category:edit", "category:delete",
          "map:map",
          "metadata:view", "metadata:create", "metadata:edit", "metadata:delete",
          "news:view", "news:create", "news:edit", "news:delete",
          "publication:view", "publication:create", "publication:edit", "publication:delete",
          "role:view", "role:create", "role:edit", "role:delete", "role:configuration",
          "user:view", "user:create", "user:edit", "user:delete",
        ],
      },
      {
        role: 'compiler',
        permissions: [
          "approval:approval",
          "category:view", "category:create", "category:edit", "category:delete",
          "map:map",
          "metadata:view", "metadata:create", "metadata:edit", "metadata:delete",
          "news:view", "news:create", "news:edit", "news:delete",
          "publication:view", "publication:create", "publication:edit", "publication:delete",
        ],
      },
      {
        role: 'executive',
        permissions: [
          "approval:approval",
          "news:view",
          "publication:view",
        ],
      },
    ];

    this.setState({ rolePermissions });
  }

  render() {
    const { roles, rolePermissions, } = this.state;

    // data permissions
    const data = [].concat.apply([], permissionData.map(({ feature, name, permissions }) => {
      return [
        { id: feature, name, isFeature: true, },
        ...permissions.map(permId => ({
          id: `${feature}:${permId}`,
          name: `${permId}`,
          isFeature: false,
        })),
      ];
    }));

    // table columns
    const colSpan = roles.length;
    const columns = [
      {
        title: <b>Permissions</b>,
        dataIndex: 'id',
        key: 'id',
        render: (id, { name, isFeature }) => {
          if (isFeature) {
            return <b>{name}</b>;
          }
          else {
            return <div className="pl3">{name}</div>;
          }
        },
      },
    ];

    roles.forEach(({ code, name }) => {
      const rolePermission = rolePermissions.find(({ role }) => role === code);

      columns.push({
        title: name,
        render: ({ id, name, isFeature }) => {
          if (isFeature) {
            return false;
          }

          let checked = false;

          if (rolePermission) {
            checked = rolePermission.permissions.indexOf(id) !== -1;
          }

          return (
            <div>
              <Switch size="small" checked={checked} />
            </div>
          );
        },
      });
    });

    return (
      <Layout>
        <PageHeader
          title="Role Configuration"
          action={
            <Button type="primary">
              <Icon type="check" /> Save
            </Button>
          }
        />

        <Table columns={columns} dataSource={data} pagination={false} size="small" />
      </Layout>
    );
  }
}

export default page(PageRoleIndex);
