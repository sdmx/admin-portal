import React from 'react';
import { Icon, message } from 'antd';

import Layout from '~/components/Layout/Admin';
import Loading from '~/components/Loading';
import Auth from '~/lib/auth';
import { Content } from '~/lib/api';
import page from '~/page';

class PageIndex extends React.Component {
  static getInitialProps({ res }) {
    if (res) {
      // res.finished = true;
      Auth.redirectIfNotAuthenticated(res);
    }

    return {};
  }

  constructor(props) {
    super(props);
    this.state = {
      news: {
        data: 0,
        loading: true,
      },
      publication: {
        data: 0,
        loading: true,
      },
    };
  }

  componentDidMount() {
    Content.contentType.forEach(({ id }) => {
      Content.list(id)
        .then(({ data }) => this.setState({
          [id]: { data: data.totalCount, loading: false },
        }))
        .catch((err) => {
          this.setState({ [id]: { data: 0, loading: false } });
        });
    });
  }

  render() {
    const { news, publication } = this.state;

    return (
      <Layout>
        <h1 className="ma0">Dashboard</h1>
        <hr className="mt0" />

        <Loading isFinish={!news.loading && !publication.loading}>
          <div className="flex">
            {Content.contentType.map(({ id, icon, text, className }) => (
              <div key={id} className={`flex pointer grow ma2 br2 pa3 white f3 ${className}`}>
                <Icon type={icon} className="f1 ph2" />
                <div className="pv2">
                  <b>{text}: {this.state[id].data}</b>
                </div>
              </div>
            ))}
          </div>
        </Loading>
      </Layout>
    );
  }
}

export default page(PageIndex);
