import React from 'react';
import PropType from 'prop-types';
import NextLink from 'next/link';
import { Button } from 'antd';

const Link = ({
  href,
  children,
  button,
  prefetch,
  ...props
}) => (
  <NextLink href={href}>
    {button
      ? (<Button href={href} {...props}>{children}</Button>)
      : (<a href={href} className="black-70" {...props}>{ children }</a>)
    }
  </NextLink>
);

Link.propTypes = {
  href: PropType.string.isRequired,
  children: PropType.node,
  button: PropType.bool,
};

Link.defaultProps = {
  button: false,
  children: '',
};

export default Link;
