import React from 'react';
import _ from 'lodash';
import axios from 'axios';
import { Select, Button, Popconfirm } from 'antd';
import { Field, reduxForm } from 'redux-form';

import Input from '~/components/Form/Input';
import PageHeader from '~/components/PageHeader';

import FormField from './FormField';
import InputThumbnail from './InputThumbnail';
import InputAttachment from './InputAttachment';

class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      links: [],
      categoryOptions: [],
    };

    this.addLink = this.addLink.bind(this);
    this.removeLink = this.removeLink.bind(this);
    this.fetchCategories = this.fetchCategories.bind(this);
  }

  componentWillMount() {
    this.fetchCategories();

    if (this.props.data) {
      const { content = {}, categories = [] } = this.props.data;

      this.props.initialize({ ...content, categoryIds: categories.map(c => c.id) });
      this.addLink(content.links);
    }
    else {
      this.addLink();
    }
  }

  fetchCategories() {
    axios.get(`${process.env.CMS_ENDPOINT}/category`).then(({ data }) => {
      this.setState({
        categoryOptions: data.data.map(category => ({
          text: category.name,
          value: category.id,
        })),
      });
    });
  }

  addLink(newLinks) {
    const { links } = this.state;
    const counter = links.length + 1;

    if (newLinks instanceof Array) {
      newLinks.map((url, i) => links.push({ id: counter + i }));
    }
    else {
      links.push({ id: counter });
    }

    this.setState({ links });
  }

  removeLink(id) {
    this.setState({ links: _.filter(this.state.links, i => i.id !== id) });
  }

  renderField(param) {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type,
      meta: { touched, error },
      ...attrs
    } = param;

    const { ele, ...customAttrs } = attrs;

    return (
      <FormField label={label}>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </FormField>
    );
  }

  render() {
    const { data, id, title, handleSubmit, onSubmit } = this.props;
    const { links, categoryOptions } = this.state;

    return (
      <div>
        <PageHeader
          title={title}
          action={
            <div>
              <Popconfirm
                placement="bottomRight"
                title="Are you sure ?"
                onConfirm={handleSubmit(values => onSubmit({ ...values, approve: false }))}
              >
                <Button htmlType="submit" type="danger" icon="close" className="ml2">
                  Reject
                </Button>
              </Popconfirm>
              <Popconfirm
                placement="bottomRight"
                title="Are you sure ?"
                onConfirm={handleSubmit(values => onSubmit({ ...values, approve: true }))}
              >
                <Button htmlType="submit" type="primary" icon="check" className="ml2">
                  Approve
                </Button>
              </Popconfirm>
            </div>
          }
        />

        <Field
          type="textarea"
          name="notes"
          label="Notes"
          component={this.renderField}
          placeholder="Notes"
          autosize={{ minRows: 2, maxRows: 6 }}
        />
        <hr size="1" />

        <div className="flex">
          <div className="w-50">
            <Field
              type="text"
              name="name"
              label="Title"
              component={this.renderField}
            />
            <Field
              type="editor"
              name="body"
              label="Content"
              component={this.renderField}
            />
            <FormField
              label="Links"
              action={(
                <Button
                  type="default"
                  icon="plus"
                  size="small"
                  className="mr2"
                  onClick={() => this.addLink()}
                />
              )}
            >
              {links.map((item, i) => (
                <div key={item.id} className="flex justify-between mt2">
                  <Field
                    type="text"
                    name={`links[${i}]`}
                    placeholder="http://domain.com"
                    component={this.renderField}
                  />

                  {links.length > 1 && (
                    <Popconfirm
                      placement="left"
                      title="Are you sure ?"
                      onConfirm={() => this.removeLink(item.id)}
                    >
                      <Button type="default" icon="close" className="mr2" />
                    </Popconfirm>
                  )}
                </div>
              ))}
            </FormField>
          </div>

          <div className="w-50 pl4">
            <Field
              type="text"
              name="categoryIds"
              label="Categories"
              component={this.renderField}
              ele={({ input }) => {
                const { value, ...attrs } = input;
                return (
                  <Select
                    {...attrs}
                    value={_.isArray(value) ? value : []}
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="Please select"
                  >
                    {categoryOptions.map(opt => (
                      <Select.Option key={opt.value} value={opt.value}>{opt.text}</Select.Option>
                    ))}
                  </Select>
                );
              }}
            />
            {/*<Field
              type="text"
              name="attributes[release_code]"
              label="Nomor Terbitan"
              component={this.renderField}
            />*/}
            <Field
              type="image"
              name="thumbnail"
              label="Thumbnail"
              component={this.renderField}
              ele={({ input }) => {
                const imgUrl = data
                  ? `${process.env.CMS_ENDPOINT}/file/${data.content.thumbnail}`
                  : null;

                return (<InputThumbnail {...input} preview={imgUrl} />);
              }}
            />
            <Field
              type="dragger"
              name="files"
              label="Attachments"
              component={this.renderField}
              ele={({ input }) => {
                const fileList = [];

                if (data) {
                  data.attachments.map(({ id, displayName, filepath }) => {
                    fileList.push({
                      uid: id,
                      name: displayName,
                      url: `${process.env.CMS_ENDPOINT}/file/${filepath}`,
                      status: 'done',
                    });
                  });
                }

                return <InputAttachment {...input} files={fileList} contentId={id} />;
              }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default reduxForm({ form: 'content' })(FormInput);
