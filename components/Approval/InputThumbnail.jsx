import React from 'react';
import { Upload, Icon, message } from 'antd';

class InputThumbnail extends React.Component {
  constructor(props) {
    super(props);
    const { preview } = props;

    this.state = {
      loading: false,
      preview: preview,
    };
  }

  getBase64(img, callback) {
    const reader = new FileReader();
    reader.addEventListener('load', () => callback(reader.result));
    reader.readAsDataURL(img);
  }

  validate() {
    // do the validation

    return false; // prevent auto upload
  }

  setPreview(file) {
    this.setState({ loading: true })

    if (file) {
      this.getBase64(file, imageUrl => this.setState({
        preview: imageUrl,
        loading: false,
      }));
    }
    else {
      this.setState({
        preview: "error",
        loading: false,
      })
    }
  }

  render() {
    const { onChange, ...attrs } = this.props;
    const { preview, loading } = this.state;

    return (
      <Upload
        listType="picture-card"
        beforeUpload={this.validate}
        showUploadList={false}
        onChange={info => {
          this.setPreview(info.file);

          if (!!onChange) {
            onChange(info.file)
          }
        }}
        {...attrs}
      >
        {!loading && preview ? (
          <img src={preview} alt={preview} />
        ) : (
          <div>
            <Icon type={loading ? 'loading' : 'plus'} className="f3" />
            <div className="ant-upload-text">Upload</div>
          </div>
        )}
      </Upload>
    );
  }
}

export default InputThumbnail;
