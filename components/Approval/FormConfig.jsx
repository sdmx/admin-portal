import React from 'react';
import _ from 'lodash';
import { connect } from 'react-redux';
import { reduxForm, Field } from 'redux-form';
import { Button, Select, Popconfirm, Icon, Input, Checkbox } from 'antd';

class FormConfig extends React.PureComponent {
  static defaultProps = {
    formValues: {
      flow: {
        id: [],
        org_id: [],
        pos_id: [],
        is_mandatory: [],
      },
    },
  }

  constructor(props) {
    super(props);
    this.state = {
      form: [],
    };

    this.addFlow = this.addFlow.bind(this);
    this.removeFlow = this.removeFlow.bind(this);
  }

  componentDidMount() {
    const { orgId, posId, data = { details: [] } } = this.props;
    const flow = {
      org_id: [],
      pos_id: [],
      is_mandatory: [],
    };

    data.details.forEach((item) => {
      flow.org_id.push(item.orgId);
      flow.pos_id.push(item.posId);
      flow.is_mandatory.push(item.isMandatory);
    });

    this.addFlow(data.details.length > 0 ? data.details.length : 1);
    this.props.initialize({ org_id: orgId, pos_id: posId, flow });
  }

  addFlow(count = 1) {
    const { form } = this.state;
    const id = (_.last(form) || 0) + 1;

    for (let i = 0; i < count; i += 1) {
      form.push(id + i);
    }

    this.setState({ form: [...form] });
  }

  removeFlow(id) {
    const { formValues: { flow }, change } = this.props;
    const index = _.findIndex(this.state.form, i => i === id);

    if (index !== -1) {
      flow.org_id.splice(index, 1);
      flow.pos_id.splice(index, 1);
      flow.is_mandatory.splice(index, 1);

      change('flow', { ...flow });

      this.setState({ form: _.filter(this.state.form, i => i !== id) });
    }
  }

  renderField(param) {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type = 'text',
      meta: { touched, error },
      ...attrs
    } = param;

    const { ele, ...customAttrs } = attrs;

    return (
      <div>
        <label htmlFor={inputAttrs.name} className="tr pv2">{label}</label>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </div>
    );
  }

  render() {
    const { handleSubmit, organisations = [], positions = [] } = this.props;
    const { form } = this.state;

    return (
      <form onSubmit={handleSubmit}>
        {form.map((id, i) => (
          <div className="dt w-100" key={id}>
            <div className="dtc w-50 pa2">
              <Field
                name={`flow[org_id][${i}]`}
                label="Organisation"
                component={this.renderField}
                ele={({ input }) => (
                  <Select {...input} placeholder="Select..." style={{ width: '100%' }}>
                    {organisations.map(d => (
                      <Select.Option key={d.value} value={d.value}>{d.text}</Select.Option>
                    ))}
                  </Select>
                )}
              />
            </div>
            <div className="dtc w-30 pa2">
              <Field
                name={`flow[pos_id][${i}]`}
                label="Position"
                component={this.renderField}
                ele={({ input }) => (
                  <Select {...input} placeholder="Select..." style={{ width: '100%' }}>
                    {positions.map(d => (
                      <Select.Option key={d.value} value={d.value}>{d.text}</Select.Option>
                    ))}
                  </Select>
                )}
              />
            </div>
            <div className="dtc w-10 pa2 pb3 v-btm nowrap">
              <Field
                name={`flow[is_mandatory][${i}]`}
                component={this.renderField}
                ele={({ input }) => (
                  <Checkbox {...input} checked={!!input.value}>Mandatory</Checkbox>
                )}
              />
            </div>
            <div className="dtc w-10 pa2 pb3 v-btm tr">
              <Popconfirm placement="left" title="Are you sure?" onConfirm={() => this.removeFlow(id)}>
                <Button type="danger">
                  <Icon type="close" />
                </Button>
              </Popconfirm>
            </div>
          </div>
        ))}

        <div className="pt2 tr">
          <Button type="secondary" className="ml2" onClick={() => this.addFlow()}>
            <Icon type="plus" /> Add
          </Button>
          <Button htmlType="submit" type="primary" className="ml2">Save</Button>
        </div>
      </form>
    );
  }
}

export default connect(({ form }) => ({
  formValues: _.get(form.approval, 'values'),
}))(reduxForm({ form: 'approval' })(FormConfig));

export const formData = ({ flow, ...values }) => {
  const data = new FormData();

  _.get(flow, 'org_id', []).forEach((orgId, i) => {
    data.append(`flow[org_id][${i}]`, orgId);
    data.append(`flow[pos_id][${i}]`, flow.pos_id[i]);
    data.append(`flow[is_mandatory][${i}]`, !!flow.is_mandatory[i]);
  });

  Object.keys(values).map(key => data.append(key, values[key]));

  return data;
};
