import React from 'react';
import { Upload, Icon, message } from 'antd';
import { List } from 'immutable';
import axios from 'axios';

import app from '~/config/app.json';

class InputAttachment extends React.Component {
  constructor(props) {
    super(props);

    const { files } = props;

    this.state = {
      files: files,
    };

    this.validate = this.validate.bind(this);
  }

  validate(file) {
    this.setState(({ files }) => ({ files: [ ...files, file ] }));

    // do the validation

    return false; // prevent auto upload
  }

  render() {
    const { contentId, ...attrs } = this.props;
    const { files } = this.state;

    return (
      <Upload.Dragger
        multiple={true}
        beforeUpload={this.validate}
        fileList={files}
        onRemove={(file) => {
          if (confirm("Are you sure ?")) {
            axios
              .delete(`${app.cms.endpoint}/content/publication/${contentId}/attachment/${file.uid}`)
              .then(() => {
                this.setState(({ files }) => ({
                  files: List(files).delete(files.indexOf(file)),
                }));
              })
              .catch(err => message.error(err.message))
          }
        }}
        {...attrs}
      >
        <p className="ant-upload-drag-icon">
          <Icon type="upload" />
        </p>
        <p className="ant-upload-text">Click or drag file to upload</p>
      </Upload.Dragger>
    );
  }
}

export default InputAttachment;
