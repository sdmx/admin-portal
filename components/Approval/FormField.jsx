import React from 'react';

export default ({ label, action, children }) => (
  <div className="w-100">
    {label && (
      <div className="pv2 flex justify-between">
        <div className="pt2 f5 b">{label}</div>
        {action && <div>{action}</div>}
      </div>
    )}
    {children}
  </div>
);
