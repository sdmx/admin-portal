import FormPublication from './FormPublication';

export default {
  Publication: FormPublication,
  News: FormPublication,
  Legal: FormPublication,
};

export const formData = (values) => {
  const form = new FormData();
  form.append('owner', 0);

  Object.keys(values).forEach((key) => {
    const value = values[key];

    switch (key) {
      case 'files':
        value.fileList.map(file => form.append(key, file.originFileObj));
        break;

      case 'thumbnail':
        if (value instanceof File) {
          form.append(key, value);
        }
        break;

      case 'links':
        form.append(key, value.filter(val => val.length > 0).join(','));
        break;

      case 'categoryIds':
        value.map(id => form.append('categoryIds[]', id));
        break;

      case 'attributes':
        form.append(key, JSON.stringify(value));
        break;

      default:
        form.append(key, value);
        break;
    }
  });

  return form;
};
