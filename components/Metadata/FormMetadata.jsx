import React from 'react';
import { reduxForm } from 'redux-form';
import { Button } from 'antd';
import moment from 'moment';

import Masonry from '~/components/Layout/Masonry';
import StructureField from './StructureField';

class FormInput extends React.PureComponent {
  componentDidMount() {
    const { initialize, data } = this.props;
    const initData = {
      content: this.recursiveMap(data.metadataset),
    };

    initialize(initData);
  }

  recursiveMap = (metadataset = []) => {
    let content = {};

    metadataset.forEach(({ id, type, value, child = [] }) => {
      switch (type.toLowerCase()) {
        case 'datetime':
          content[id] = moment(value, 'x').format('YYYY-MM-DD');
          break;

        default:
          content[id] = value;
          break;
      }

      if (child.length > 0) {
        content = { ...content, ...this.recursiveMap(child) };
      }
    });

    return content;
  }

  render() {
    const { handleSubmit, msd = { structure: [] }, data = {} } = this.props;

    return (
      <div>
        <h1>{`${msd.name} (${msd.id})`}</h1>
        <hr />

        <form onSubmit={handleSubmit}>
          <div className="tr pb2">
            <Button htmlType="submit" type="primary">Save</Button>
          </div>

          <Masonry
            data={msd.structure}
            cellRenderer={content => (
              <StructureField data={data} structure={content} />
            )}
          />
        </form>
      </div>
    );
  }
}

FormInput.defaultProps = {
  msd: { structure: [] },
  data: {},
};

export default reduxForm({ form: 'metadata' })(FormInput);

export const formData = (values) => {
  const form = new FormData();

  Object.keys(values.content).forEach(key => form.append(`content[${key}]`, values.content[key]));

  return form;
};
