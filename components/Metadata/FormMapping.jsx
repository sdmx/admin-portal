import React from 'react';
import { reduxForm, Field } from 'redux-form';
import { Button, Form, Input, Select } from 'antd';

class FormInput extends React.PureComponent {
  componentDidMount() {
    const { initialize, data } = this.props;
    console.log(data);
    initialize(data);
  }

  renderField = (param) => {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type,
      meta: { touched, error },
      ...attrs
    } = param;

    const { ele, ...customAttrs } = attrs;

    return (
      <Form.Item label={label} labelFor={inputAttrs.name}>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </Form.Item>
    );
  }

  render() {
    const { handleSubmit, id, msdList = [] } = this.props;

    return (
      <div>
        <h1>{`${id}`}</h1>
        <hr />

        <form onSubmit={handleSubmit} className="w-50">
          <Field
            type="select"
            name="msdId"
            label="Metadata"
            component={this.renderField}
            ele={({ input }) => (
              <Select {...input} placeholder="Select..." style={{ width: '100%' }}>
                {msdList.map(msd => (
                  <Select.Option key={msd.id} value={msd.id}>
                    {`${msd.id} - ${msd.name}`}
                  </Select.Option>
                ))}
              </Select>
            )}
          />

          <div className="tr pb2">
            <Button htmlType="submit" type="primary">Save</Button>
          </div>
        </form>
      </div>
    );
  }
}

FormInput.defaultProps = {
  msdList: [],
  data: {},
};

export default reduxForm({ form: 'metadata' })(FormInput);
