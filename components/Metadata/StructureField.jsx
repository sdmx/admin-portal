import React from 'react';
import { Field } from 'redux-form';
import { Form, Select, Spin } from 'antd';

import Input from '~/components/Form/Input';

class StructureField extends React.PureComponent {
  renderField = (param) => {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type,
      meta: { touched, error },
      ...attrs
    } = param;

    const { ele, ...customAttrs } = attrs;

    const formItemLayout = {
      // labelCol: {
      //   xs: { span: 24 },
      //   sm: { span: 8 },
      // },
      // wrapperCol: {
      //   xs: { span: 24 },
      //   sm: { span: 16 },
      // },
    };

    return (
      <Form.Item label={label} labelFor={inputAttrs.name} {...formItemLayout}>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </Form.Item>
    );
  }

  renderStructureField(id, label, type) {
    const fieldAttrs = {};

    switch (type.toLowerCase()) {
      case 'datetime':
        fieldAttrs.type = 'date';
        break;

      case 'enumeration':
        fieldAttrs.type = 'text';
        break;

      default:
        fieldAttrs.type = 'text';
        break;
    }

    return (
      <Field
        key={id}
        type="text"
        name={`content[${id}]`}
        label={label}
        component={this.renderField}
        {...fieldAttrs}
      />
    );
  }

  render() {
    const { structure: { id, label, type, structure = [] } } = this.props;

    if (structure.length > 0) {
      return (
        <div className="br2 pb2 bg-white ba b--moon-gray">
          <div className="bg-light-gray pa2 bb b--moon-gray">
            {this.renderStructureField(id, label, type)}
          </div>
          <div className="pa2">
            {structure.map(item => (
              <StructureField structure={item} />
            ))}
          </div>
        </div>
      );
    }

    return (
      <div className="pl2 pb2 bg-white bb b--moon-gray">
        {this.renderStructureField(id, label, type)}
      </div>
    );
  }
}

// export default reduxForm({ form: 'metadata' })(StructureField);
export default StructureField;
