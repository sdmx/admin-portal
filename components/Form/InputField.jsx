import React from 'react';
import { Form } from 'antd';
import { Field } from 'redux-form';
import InputComponent from './Input';

export class Input extends React.Component {
  constructor(props) {
    super(props);
    this.createInputComponent = this.createInputComponent.bind(this);
  }

  createInputComponent({ input }) {
    const inputProps = this.props;

    return (
      <InputComponent
        value={input.value}
        onChange={input.onChange}
        {...inputProps}
      />
    );
  }

  render() {
    return (
      <Field
        name={this.props.name}
        component={this.createInputComponent}
      />
    );
  }
}

export const FormGroup = ({ name, label, ...formProps }) => (
  <Form.Item label={label && name}>
    <Input name={name} {...formProps} />
  </Form.Item>
);
