import TableUserDb from './TableUserDb';
import TableUserLdap from './TableUserLdap';

export default {
  db: TableUserDb,
  ldap: TableUserLdap,
};
