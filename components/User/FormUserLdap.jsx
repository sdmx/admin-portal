import React from 'react';
import _ from 'lodash';
import validator from 'validator';
import { Select, Button, message } from 'antd';
import { Field, reduxForm } from 'redux-form';

import { Auth, Organisation } from '~/lib/api';
import Input from '~/components/Form/Input';
import PageHeader from '~/components/PageHeader';

import FormField from './FormField';

const renderField = (param) => {
  const {
    input: { onBlur, ...inputAttrs },
    label,
    type,
    meta: { touched, error },
    ...attrs
  } = param;

  const { ele, ...customAttrs } = attrs;

  return (
    <FormField label={label}>
      {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
      {touched && error && <div className="dark-red">{error}</div>}
    </FormField>
  );
};

class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
      organisations: [],
      positions: [],
    };
  }

  componentDidMount() {
    // fetch roles
    Auth.getRoles().then(({ data }) => {
      this.setState({
        roles: data.map(role => ({ text: role.name, value: role.code })),
      });
    });

    // fetch organisations
    Organisation.list()
      .then(({ data }) => this.setState({
        organisations: data.data.map(i => ({ text: i.name, value: i.id })),
      }))
      .catch(err => message.error(err.message));

    // fetch position
    Organisation.positions()
      .then(({ data }) => this.setState({
        positions: data.data.map(i => ({ text: i.name, value: i.id })),
      }))
      .catch(err => message.error(err.message));

    // init form data
    const { data = {} } = this.props;
    const { password, ...formValues } = data;

    this.props.initialize(formValues);
  }

  render() {
    const { title } = this.props;
    const { roles, organisations, positions } = this.state;

    return (
      <form onSubmit={this.props.handleSubmit}>
        <PageHeader
          title={title}
          action={<Button htmlType="submit" type="primary">Save</Button>}
        />

        <div className="flex">
          <div className="w-40">
            <Field
              type="text"
              name="uid"
              label="Username"
              component={renderField}
            />
            <Field
              type="text"
              name="name"
              label="Name"
              component={renderField}
            />
            <Field
              type="text"
              name="email"
              label="Email"
              component={renderField}
            />
          </div>

          <div className="w-60 pl4">
            <Field
              type="text"
              name="roles"
              label="Roles"
              component={renderField}
              ele={({ input }) => {
                const { value, ...attrs } = input;
                return (
                  <Select
                    {...attrs}
                    value={_.isArray(value) ? value : []}
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="Please select"
                  >
                    {roles.map(opt => (
                      <Select.Option key={opt.value} value={opt.value}>{opt.text}</Select.Option>
                    ))}
                  </Select>
                );
              }}
            />

            <div className="flex">
              <div className="w-50 pr4">
                <Field
                  type="password"
                  name="password"
                  label="Password"
                  component={renderField}
                />
              </div>
              <div className="w-50">
                <Field
                  type="password"
                  name="passwordConfirm"
                  label="Confirm Password"
                  component={renderField}
                />
              </div>
            </div>

            <div className="flex">
              <div className="w-50 pr4">
                <Field
                  type="select"
                  name="orgId"
                  label="Organisation"
                  component={renderField}
                  options={organisations}
                />
              </div>
              <div className="w-50">
                <Field
                  type="select"
                  name="posId"
                  label="Position"
                  component={renderField}
                  options={positions}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const validate = ({ uid, name, email, password, passwordConfirm }) => {
  const errors = {};

  if (!uid) {
    errors.uid = 'Required';
  }

  if (!name) {
    errors.name = 'Required';
  }

  if (!email || !validator.isEmail(email)) {
    errors.email = 'Email is invalid';
  }

  if (password) {
    if (password !== passwordConfirm) {
      errors.passwordConfirm = 'Password doesn\'t match';
    }

    // if (!validator.isLength(password, { min: 6 })) {
    //   errors.password = 'Password must at least 6 characters';
    // }

    // if (!password.match(/[A-Z]/)) {
    //   errors.password = 'Password must contain at least one capital letter';
    // }

    // if (!password.match(/\d/)) {
    //   errors.password = 'Password must contain number';
    // }

    // if (!password.match(/\W/)) {
    //   errors.password = 'Password must contain at least one special character';
    // }
  }

  return errors;
};

export default reduxForm({ form: 'content', validate })(FormInput);
