import React from 'react';
import _ from 'lodash';
import axios from 'axios';
import validator from 'validator';
import { Select, Button } from 'antd';
import { Field, reduxForm } from 'redux-form';

import Input from '~/components/Form/Input';
import PageHeader from '~/components/PageHeader';

import FormField from './FormField';

const renderField = (param) => {
  const {
    input: { onBlur, ...inputAttrs },
    label,
    type,
    meta: { touched, error },
    ...attrs
  } = param;

  const { ele, ...customAttrs } = attrs;

  return (
    <FormField label={label}>
      {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
      {touched && error && <div className="dark-red">{error}</div>}
    </FormField>
  );
};

class FormInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      roles: [],
    };

    this.fetchRoles = this.fetchRoles.bind(this);
  }

  componentDidMount() {
    const { data = {} } = this.props;
    const { password, ...formValues } = data;

    this.fetchRoles();
    this.props.initialize(formValues);
  }

  fetchRoles() {
    axios.get(`${process.env.AUTH_ENDPOINT}/auth/roles`).then(({ data }) => {
      this.setState({
        roles: data.map(role => ({ text: role.name, value: role.code })),
      });
    });
  }

  render() {
    const { title } = this.props;
    const { roles } = this.state;

    return (
      <form onSubmit={this.props.handleSubmit}>
        <PageHeader
          title={title}
          action={<Button htmlType="submit" type="primary">Save</Button>}
        />

        <div className="flex">
          <div className="w-40">
            <Field
              type="text"
              name="name"
              label="Name"
              component={renderField}
            />

            <Field
              type="text"
              name="email"
              label="Email"
              component={renderField}
            />
          </div>

          <div className="w-60 pl4">
            <Field
              type="text"
              name="roles"
              label="Roles"
              ele={({ input }) => {
                const { value, ...attrs } = input;
                return (
                  <Select
                    {...attrs}
                    value={_.isArray(value) ? value : []}
                    mode="multiple"
                    style={{ width: '100%' }}
                    placeholder="Please select"
                  >
                    {roles.map(opt => (
                      <Select.Option key={opt.value} value={opt.value}>{opt.text}</Select.Option>
                    ))}
                  </Select>
                );
              }}
              component={renderField}
            />

            <div className="flex">
              <div className="w-50 pr4">
                <Field
                  type="password"
                  name="password"
                  label="Password"
                  component={renderField}
                />
              </div>
              <div className="w-50">
                <Field
                  type="password"
                  name="passwordConfirm"
                  label="Confirm Password"
                  component={renderField}
                />
              </div>
            </div>
          </div>
        </div>
      </form>
    );
  }
}

const validate = ({ name, email, password, passwordConfirm }) => {
  const errors = {};

  if (!name) {
    errors.name = 'Required';
  }

  if (!email || !validator.isEmail(email)) {
    errors.email = 'Email is invalid';
  }

  if (password) {
    if (password !== passwordConfirm) {
      errors.passwordConfirm = 'Password doesn\'t match';
    }

    if (!validator.isLength(password, { min: 6 })) {
      errors.password = 'Password must at least 6 characters';
    }

    if (!password.match(/[A-Z]/)) {
      errors.password = 'Password must contain at least one capital letter';
    }

    if (!password.match(/\d/)) {
      errors.password = 'Password must contain number';
    }

    if (!password.match(/\W/)) {
      errors.password = 'Password must contain at least one special character';
    }
  }

  return errors;
};

export default reduxForm({ form: 'content', validate })(FormInput);
