import React from 'react';
import { Popconfirm, Button, message } from 'antd';
import Table from 'react-table';
import axios from 'axios';
import moment from 'moment';
import nprogress from 'nprogress';
import _ from 'lodash';

import Link from '~/components/Link';

class TableUser extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: true,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page }) {
    this.setState({ loading: true });

    axios
      .get(this.props.endpoint, {
        params: {
          limit: pageSize,
          offset: page * pageSize,
        },
      })
      .then(({ data }) => {
        this.setState({
          loading: false,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
        });
      })
      .catch(() => this.setState({ loading: false }));
  }

  render() {
    const { userType, endpoint } = this.props;
    const { data, totalPages, loading } = this.state;

    return (
      <Table
        manual
        filterable
        pageSize={10}
        defaultPageSize={10}
        data={data}
        pages={totalPages}
        loading={loading}
        onFetchData={this.fetchData}
        className="f6"
        columns={[
          { Header: 'Username', accessor: 'uid' },
          { Header: 'Name', accessor: 'name' },
          { Header: 'Email', accessor: 'email' },
          {
            Header: 'Last Login',
            accessor: 'last_login',
            className: 'tc',
            width: 150,
            Cell: cell => moment(cell.original.last_login).format('DD MMM YYYY'),
          },
          {
            Header: 'Action',
            width: 100,
            Cell: cell => (
              <div className="tc">
                <Link
                  button
                  href={`/user/${userType}/edit/${cell.original.uid}`}
                  type="default"
                  icon="edit"
                  className="mr2"
                />
                <Popconfirm
                  placement="left"
                  title="Are you sure ?"
                  onConfirm={() => {
                    nprogress.start();

                    axios
                      .delete(`${endpoint}/${cell.original.uid}`)
                      .then(() => this.setState({
                        data: _.filter(data, item => item.uid !== cell.original.uid),
                      }, () => nprogress.done()))
                      .catch((err) => {
                        message.error(err.message);
                        nprogress.done();
                      });
                  }}
                >
                  <Button type="danger" icon="delete" />
                </Popconfirm>
              </div>
            ),
          },
        ]}
      />
    );
  }
}

export default TableUser;
