import React from 'react';
import { Popconfirm, Button, message } from 'antd';
import Table from 'react-table';
import axios from 'axios';
import moment from 'moment';
import nprogress from 'nprogress';
import _ from 'lodash';

import Link from '~/components/Link';
import config from '~/lib/config';

class TableUser extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      totalPages: 1,
      loading: true,
    };

    this.fetchData = this.fetchData.bind(this);
  }

  fetchData({ pageSize, page, sorted, filtered }) {
    this.setState({ loading: true });

    axios
      .get(this.props.endpoint, {
        params: {
          limit: pageSize,
          offset: page * pageSize,
        },
      })
      .then(({ data }) => {
        this.setState({
          loading: false,
          data: data.data,
          totalPages: Math.ceil(data.totalCount / pageSize),
        });
      })
      .catch(() => this.setState({ loading: false }));
  }

  render() {
    const { userType, endpoint } = this.props;
    const { data, totalPages, loading } = this.state;

    return (
      <Table
        manual
        filterable
        pageSize={10}
        defaultPageSize={10}
        data={data}
        pages={totalPages}
        loading={loading}
        onFetchData={this.fetchData}
        className="f6"
        columns={[
          { Header: 'Name', accessor: 'name' },
          { Header: 'Email', accessor: 'email' },
          {
            Header: 'Status',
            accessor: 'status',
            className: 'tc',
            width: 100,
            Cell: ({ original }) => {
              let className;

              switch (original.status) {
                // Blocked
                case 2: className = 'bg-dark-red white'; break;

                // Active
                case 1: className = 'bg-dark-green white'; break;

                // Unverified
                default:
                case 0: className = 'bg-light-gray black-80'; break;
              }

              return (
                <div className={`pa1 br1 f7 b ${className}`}>
                  {config('status')[original.status]}
                </div>
              );
            },
          },
          {
            Header: 'Last Login',
            accessor: 'last_login',
            className: 'tc',
            width: 150,
            Cell: ({ original }) => moment(original.last_login).format('DD MMM YYYY'),
          },
          {
            Header: 'Action',
            width: 100,
            Cell: ({ original }) => (
              <div className="tc">
                <Link
                  button
                  href={`/user/${userType}/edit/${original.id}`}
                  type="default"
                  icon="edit"
                  className="mr2"
                />
                <Popconfirm
                  placement="left"
                  title="Are you sure ?"
                  onConfirm={() => {
                    nprogress.start();

                    axios
                      .delete(`${endpoint}/${original.id}`)
                      .then(() => this.setState({
                        data: _.filter(data, item => item.id !== original.id),
                      }, () => nprogress.done()))
                      .catch((err) => {
                        message.error(err.message);
                        nprogress.done();
                      });
                  }}
                >
                  <Button type="danger" icon="delete" />
                </Popconfirm>
              </div>
            ),
          },
        ]}
      />
    );
  }
}

export default TableUser;
