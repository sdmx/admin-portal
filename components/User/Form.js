import FormUserDb from './FormUserDb';
import FormUserLdap from './FormUserLdap';

export default {
  db: FormUserDb,
  ldap: FormUserLdap,
};

export const formData = (values) => {
  const form = new FormData();

  Object.keys(values).forEach((key) => {
    const value = values[key];

    switch (key) {
      case 'roles':
        value.map(id => form.append('roles[]', id));
        break;

      default:
        form.append(key, value);
        break;
    }
  });

  return form;
};
