import React from 'react';
import { Layout as AntLayout, Menu, Icon, Dropdown, message, Tooltip } from 'antd';
import Router from 'next/router';
import Nprogress from 'nprogress';

import 'react-quill/dist/quill.snow.css';
import 'antd/dist/antd.min.css';
import 'react-table/react-table.css';

import Link from '~/components/Link';
import Loading from '~/components/Loading';
import Auth from '~/lib/auth';
import { Auth as ApiAuth } from '~/lib/api';

import '~/public/static/css/nprogress.min.css';
import '~/public/static/css/tachyons.min.css';
import '~/public/static/css/override.css';
import '~/public/static/css/custom.css';

const { Header, Content, Sider } = AntLayout;

class Layout extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      collapsedLeft: false,
      collapsedRight: false,
      user: {},
      loading: true,
    };

    if (process.browser) {
      Auth.user().then(user => this.setState({ user }));
      ApiAuth.init()
        .then(() => this.setState({ loading: false }))
        .catch((err) => {
          message.error(err.message);
          this.setState({ loading: false });
        });
    }
  }

  componentDidMount() {
    Router.events.on('routeChangeStart', () => Nprogress.start());
    Router.events.on('routeChangeComplete', () => Nprogress.done());
    Router.events.on('routeChangeError', () => Nprogress.done());
  }

  render() {
    const { user, loading, collapsedLeft, collapsedRight } = this.state;
    const { children, left, top, right } = this.props;

    const subMenus = [
      { url: '/auth/logout', label: 'Logout', icon: 'logout', onClick: () => ApiAuth.logout(false) },
    ];
    const topRight = [
      { url: '/', label: user.name, icon: 'user', subs: subMenus },
    ];

    return (
      <AntLayout className="bg-white" style={{ minHeight: '100vh' }}>
        <Header className="header pa0 z-1 bg-white flex justify-between">
          <div className="flex">
            {collapsedLeft ? (
              <Link
                href="/"
                className="bg-primary tc ph3 h-100 b--silver"
                style={{ width: '70px', float: 'left' }}
              >
                <img src="/static/img/logo-inverse-icon.jpg" alt="Logo" style={{ width: '100%' }} />
              </Link>
            ) : (
              <Link
                href="/"
                className="bg-primary tc ph2 h-100 b--silver"
                style={{ width: '229px', float: 'left' }}
              >
                <img src="/static/img/logo-inverse.jpg" alt="Logo" style={{ width: '250px' }} />
              </Link>
            )}

            <Icon
              className="pointer f4 b ph3 hover-bg-moon-gray"
              type={collapsedLeft ? 'right' : 'left'}
              style={{ lineHeight: '64px' }}
              onClick={() => this.setState({ collapsedLeft: !collapsedLeft })}
            />
          </div>

          <Menu mode="horizontal" className="bg-white dib" style={{ lineHeight: '64px' }}>
            {top && top.map(item => (
              <Menu.Item key={item.label}>
                {item.html
                  ? (<a href={item.url} className="gray">{item.label}</a>)
                  : (<Link href={item.url} className="gray">{item.label}</Link>)
                }
              </Menu.Item>
            ))}
          </Menu>

          <Menu mode="horizontal" style={{ lineHeight: '64px' }}>
            {topRight && topRight.map((item, i) => (
              <Menu.Item key={i}>
                {item.subs ? (
                  <Dropdown
                    placement="bottomLeft"
                    overlay={(
                      <Menu>
                        {item.subs && item.subs.map((subitem) => {
                          const icon = subitem.icon && (
                            <Icon
                              type={subitem.icon}
                              style={{ fontSize: '16px' }}
                              className="ph1"
                            />
                          );

                          return (
                            <Menu.Item key={subitem.label}>
                              {subitem.html ? (
                                <a href={subitem.url} className="black-70 f6" onClick={subitem.onClick}>
                                  {icon}
                                  {subitem.label}
                                </a>
                              ) : (
                                <Link href={subitem.url} className="black-70 f6" onClick={subitem.onClick}>
                                  {icon}
                                  {subitem.label}
                                </Link>
                              )}
                            </Menu.Item>
                          );
                        })}
                      </Menu>
                    )}
                  >
                    <div className="black-70 pointer ph3 f6">
                      {item.icon && (
                        <Icon type={item.icon} style={{ fontSize: '18px' }} className="ph1" />
                      )}
                      {item.label}
                    </div>
                  </Dropdown>
                ) : (
                  <Link href={item.url} onClick={item.onClick}>{item.label}</Link>
                )}
              </Menu.Item>
            ))}
          </Menu>
        </Header>

        <Loading isFinish={!loading}>
          {/* Left Sidebar */}
          <AntLayout>
            {left && (
              <Sider
                collapsible
                trigger={null}
                width={collapsedLeft ? 71 : 230}
                className="br b--light-gray bg-primary pt3"
              >
                <Content>
                  <Menu
                    mode="inline"
                    selectedKeys={['1']}
                    className="bg-primary h-100 br-0"
                  >
                    {left && left.map(item => (
                      <Menu.Item key={item.label}>
                        <Tooltip title={collapsedLeft ? item.label : null} placement="right">
                          <Link href={item.url} className="default">
                            {item.icon && (
                              <Icon
                                type={item.icon}
                                style={collapsedLeft
                                  ? { fontSize: '30px' }
                                  : { fontSize: '15px' }
                                }
                              />
                            )}

                            <span>
                              {!collapsedLeft && item.label}
                            </span>
                          </Link>
                        </Tooltip>
                      </Menu.Item>
                    ))}
                  </Menu>
                </Content>
              </Sider>
            )}

            {/* Content */}
            <AntLayout className="pt3 pa4 pb5 bg-white">
              <div style={{ minHeight: 280 }}>
                {children}
              </div>
            </AntLayout>

            {/* Right Sidebar */}
            {right && (
              <Sider
                collapsible
                collapsed={collapsedRight}
                trigger={null}
                width={230}
                className="br b--light-gray bg-white"
              >
                <Icon
                  className="absolute pointer pv4 ph1 dark-blue hover-navy bg-white bt bb bl b--light-gray"
                  type={!collapsedRight ? 'right' : 'left'}
                  onClick={() => this.setState({ collapsedRight: !collapsedRight })}
                  style={{ right: '100%', top: '40%' }}
                />
                <Content>
                  <div style={{ height: '500px', overflowY: 'auto', overflowX: 'hidden' }}>
                    <div dangerouslySetInnerHTML={{ __html: right }} />
                  </div>
                </Content>
              </Sider>
            )}
          </AntLayout>
        </Loading>
      </AntLayout>
    );
  }
}

// export default connect(state => state)(Layout);
export default Layout;
