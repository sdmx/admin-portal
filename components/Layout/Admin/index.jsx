import React from 'react';

import Layout from '~/components/Layout';

class AdminLayout extends React.Component {
  render() {
    const { children } = this.props;
    const leftMenu = [
      { url: '/', icon: 'home', label: 'Home' },
      { url: '/user/ldap', icon: 'user', label: 'Users' },
      { url: '/role', icon: 'idcard', label: 'Roles' },
      { url: '/category', icon: 'appstore', label: 'Categories' },
      { url: '/content/news/public', icon: 'file-text', label: 'News' },
      { url: '/content/publication/public', icon: 'sound', label: 'Publications' },
      { url: '/approval', icon: 'check-square', label: 'Approvals' },
      { url: '/metadata', icon: 'file-protect', label: 'Metadata' },
      { url: '/geo', icon: 'environment', label: 'Map' },
    ];

    return (
      <Layout left={leftMenu}>
        {children}
      </Layout>
    );
  }
}

export default AdminLayout;
