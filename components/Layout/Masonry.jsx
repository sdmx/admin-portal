import React from 'react';
import {
  CellMeasurer,
  CellMeasurerCache,
  Masonry,
  createMasonryCellPositioner,
  AutoSizer,
  WindowScroller,
} from 'react-virtualized';

const spacer = 10;
const columnCount = 2;
const columnWidth = 250;
const defaultHeight = 250;

let masonryRef;

export default class RenderMasonry extends React.Component {
  static defaultProps = {
    data: [],
  }

  constructor(props) {
    super(props);

    const cache = new CellMeasurerCache({
      defaultHeight,
      defaultWidth: columnWidth,
      fixedWidth: true,
    });

    const cellPositioner = createMasonryCellPositioner({
      cellMeasurerCache: cache,
      columnCount,
      columnWidth,
      spacer,
    });

    this.state = {
      cache,
      cellPositioner,
      itemWidth: columnWidth,
    };

    this.onResize = this.onResize.bind(this);
  }

  onResize = ({ width }) => {
    const { itemWidth, cellPositioner } = this.state;
    let newWidth = itemWidth;

    if (width) {
      newWidth = this.getColumnWidth(width);
      this.setState({ itemWidth: newWidth });
    }

    cellPositioner.reset({
      columnCount: 3,
      columnWidth: newWidth,
      spacer: 10,
    });

    masonryRef.recomputeCellPositions();
  }

  cellRenderer = ({ index, key, parent, style }) => {
    const { data, cellRenderer } = this.props;
    const { itemWidth, cache } = this.state;
    const content = data[index];
    const newStyle = { ...style };

    newStyle.width = `${itemWidth}px`;

    return (
      <CellMeasurer key={key} cache={cache} index={index} parent={parent}>
        {() => (
          <div style={newStyle}>
            {cellRenderer(content, { index, key })}
          </div>
        )}
      </CellMeasurer>
    );
  }

  getColumnWidth = width => Math.floor((width - spacer * (columnCount - 1)) / columnCount)

  render() {
    const { data = [] } = this.props;
    const { cache, cellPositioner } = this.state;

    return (
      <WindowScroller>
        {({ height, scrollTop, isScrolling }) => (
          <AutoSizer
            disableHeight
            height={height}
            scrollTop={scrollTop}
            onResize={this.onResize}
          >
            {({ width }) => (
              <Masonry
                autoHeight
                cellCount={data.length}
                cellMeasurerCache={cache}
                cellPositioner={cellPositioner}
                cellRenderer={this.cellRenderer}
                isScrolling={isScrolling}
                scrollTop={scrollTop}
                width={width}
                height={height}
                ref={(ref) => {
                  masonryRef = ref;
                }}
              />
            )}
          </AutoSizer>
        )}
      </WindowScroller>
    );
  }
}
