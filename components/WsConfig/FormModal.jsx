import React from 'react';
import { Modal, Select, Input, Form } from 'antd';
import { reduxForm, Field } from 'redux-form';

const wsType = [
  { type: 'rest', name: 'REST' },
  { type: 'soap', name: 'SOAP' },
];

const sdmxVersions = ['2.0', '2.1'];

class FormModal extends React.Component {
  static defaultProps = {
    data: {},
    show: false,
  };

  componentDidUpdate() {
    this.props.initialize(this.props.data);
  }

  renderField(param) {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type,
      meta: { touched, error },
      ...attrs
    } = param;
    const { ele, ...customAttrs } = attrs;
    console.log(param);

    return (
      <Form.Item label={label} labelFor={inputAttrs.name}>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </Form.Item>
    );
  }

  render() {
    const { show } = this.props;

    return (
      <Modal
        title="Web Service Configuration"
        okText="Save"
        visible={show}
        onOk={this.props.handleSubmit}
        onCancel={this.props.onCloseModal}
      >
        <form onSubmit={this.props.handleSubmit}>
          <div className="mv2">
            <div className="cf">
              <Field
                type="text"
                name="name"
                label="Name"
                component={this.renderField}
              />
            </div>
          </div>
          <div className="mv2">
            <div className="cf">
              <div className="fl w-50 p3">
                <Field
                  type="select"
                  name="endpoint.wsType"
                  label="Type"
                  component={this.renderField}
                  ele={({ input }) => (
                    <Select {...input} placeholder="Select..." className="w-100">
                      {wsType.map(item => (
                        <Select.Option key={item.type} value={item.type}>{item.name}</Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </div>
              <div className="fl w-50 pl3">
                <Field
                  type="select"
                  name="endpoint.version"
                  label="Version"
                  component={this.renderField}
                  ele={({ input }) => (
                    <Select {...input} placeholder="Select..." className="w-100">
                      {sdmxVersions.map(version => (
                        <Select.Option key={version} value={version}>{version}</Select.Option>
                      ))}
                    </Select>
                  )}
                />
              </div>
            </div>
          </div>
          <div className="mv2">
            <div className="cf">
              <Field
                type="text"
                name="endpoint.url"
                label="Endpoint"
                component={this.renderField} />
            </div>
          </div>
        </form>
      </Modal>
    );
  }
}

export default reduxForm({ form: 'wsConfig', validate: values => console.log(values) })(FormModal);
