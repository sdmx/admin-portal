import React from 'react';
import PropTypes from 'prop-types';
import { reduxForm, Field } from 'redux-form';
import { Button, Form, Select, Spin } from 'antd';
import axios from 'axios';

import Input from '~/components/Form/Input';

class FormInput extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { parents: [], fetching: true };
    this.fetchParentOptions = this.fetchParentOptions.bind(this);
  }

  componentDidMount() {
    this.props.initialize(this.props.data);
    this.fetchParentOptions();
  }

  fetchParentOptions() {
    axios.get(`${process.env.CMS_ENDPOINT}/category`).then(({ data }) => {
      this.setState({
        fetching: false,
        parents: data.data
          .filter(item => item.id !== this.props.id)
          .map(item => ({ text: item.name, value: item.id })),
      });
    });
  }

  renderField(param) {
    const {
      input: { onBlur, ...inputAttrs },
      label,
      type,
      meta: { touched, error },
      ...attrs
    } = param;

    const { ele, ...customAttrs } = attrs;
    console.log(inputAttrs, customAttrs)

    return (
      <Form.Item label={label} labelFor={inputAttrs.name}>
        {ele ? ele(param) : <Input type={type} {...inputAttrs} {...customAttrs} />}
        {touched && error && <div className="dark-red">{error}</div>}
      </Form.Item>
    );
  }

  render() {
    const { fetching, parents } = this.state;

    return (
      <div>
        <h1>Edit Category</h1><hr />
        <form onSubmit={this.props.handleSubmit} className="w-50">
          <Field
            type="text"
            name="name"
            label="Name"
            component={this.renderField}
          />
          <Field
            type="image"
            name="parentId"
            label="Parent"
            component={this.renderField}
            ele={({ input }) => (
              <Select
                {...input}
                placeholder="Select..."
                style={{ width: '100%' }}
                notFoundContent={fetching ? <Spin size="small" /> : null}
              >
                {parents.map(d => (
                  <Select.Option key={d.value} value={d.value}>{d.text}</Select.Option>
                ))}
              </Select>
            )}
          />

          <div className="pt2 tr">
            <Button htmlType="submit" type="primary">Save</Button>
          </div>
        </form>
      </div>
    );
  }
}

FormInput.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  data: PropTypes.objectOf(PropTypes.node),
};

FormInput.defaultProps = {
  data: {},
};

export default reduxForm({ form: 'category' })(FormInput);

export const formData = (values) => {
  const form = new FormData();

  Object.keys(values).map(key => form.append(key, values[key]));

  return form;
};
