import React from 'react';
import classnames from 'classnames';
import PropType from 'prop-types';

const Icon = ({ type }) => (
  <i className={classnames('fa', type)} />
);

Icon.propTypes = {
  type: PropType.string.isRequired,
};

export default Icon;
